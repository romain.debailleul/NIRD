#!/bin/bash
###########################################################################
# Testé uniquement sur Linux Mint 21
###########################################################################
#Merci à Rémi Debrock, Cédric Frayssinet, Georges Khaznadar, Thomas Lévêque, Emmanuel Ostenne pour leur contribution.

# Proxy system
###########################################################################
#Paramétrage par défaut
#Changez les valeurs, ainsi, il suffira de taper 'entrée' à chaque question
###########################################################################
proxy_def_ip="172.16.0.253"
proxy_def_port="3128"
proxy_gnome_noproxy="[ 'localhost', '127.0.0.0/8', '172.16.0.0/16', '192.168.0.0/16' ]"
#proxy_env_noproxy="localhost,127.0.0.1,192.168.0.0/16,172.16.0.0/16"
pagedemarragepardefaut="https://www.lilo.org/"

#############################################
# Run using sudo, of course.
#############################################
if [ "$UID" -ne "0" ]
then
  echo "Il faut etre root pour executer ce script. ==> sudo "
  exit 
fi 

#Prévoir un test de version de la distribution.

#####################################
# Existe-t-il un proxy à paramétrer ?
#####################################

read -p "Faut-il enregistrer l'utilisation d'un proxy ? [O/n] " rep_proxy
if [ "$rep_proxy" = "O" ] || [ "$rep_proxy" = "o" ] || [ "$rep_proxy" = "" ] ; then
  read -p "Donnez l'adresse ip du proxy ? [$proxy_def_ip] " ip_proxy
  if [ "$ip_proxy" = "" ] ; then
    ip_proxy=$proxy_def_ip
  fi
  read -p "Donnez le n° port du proxy ? [$proxy_def_port] " port_proxy
  if [ "$port_proxy" = "" ] ; then
    port_proxy=$proxy_def_port
  fi
else
  ip_proxy=""
  port_proxy=""
fi

###################################################
# cron d'extinction automatique à lancer ?
###################################################

echo "Pour terminer, voulez-vous activer l'extinction automatique des postes le soir ?"
echo "0 = non, pas d'extinction automatique le soir"
echo "1 = oui, extinction a 19H00"
echo "2 = oui, extinction a 20H00"
echo "3 = oui, extinction a 22H00"
read -p "Répondre par le chiffre correspondant (0,1,2,3) : " rep_proghalt

if [ "$rep_proghalt" = "1" ] ; then
        echo "0 19 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
        else if [ "$rep_proghalt" = "2" ] ; then
                echo "0 20 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                else if [ "$rep_proghalt" = "3" ] ; then
                        echo "0 22 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                     fi
             fi
fi

#######################################################
#Paramétrage des paramètres Proxy pour tout le système
#######################################################
if [[ "$ip_proxy" != "" ]] && [[ $port_proxy != "" ]] ; then

  echo "Paramétrage du proxy $ip_proxy:$port_proxy" 

#Paramétrage des paramètres Proxy pour Gnome
#######################################################
  echo "[org.gnome.system.proxy]
mode='manual'
use-same-proxy=true
ignore-hosts=$proxy_gnome_noproxy
[org.gnome.system.proxy.http]
host='$ip_proxy'
port=$port_proxy
[org.gnome.system.proxy.https]
host='$ip_proxy'
port=$port_proxy
" >> /usr/share/glib-2.0/schemas/my-defaults.gschema.override

  glib-compile-schemas /usr/share/glib-2.0/schemas

#Paramétrage du Proxy pour le système
######################################################################
  echo "http_proxy=http://$ip_proxy:$port_proxy/
https_proxy=http://$ip_proxy:$port_proxy/
ftp_proxy=http://$ip_proxy:$port_proxy/" >> /etc/profile

#Paramétrage du Proxy pour apt
######################################################################
  echo ""
  echo "Un utilisateur de préférence sans aucun droits est requis pour le protocole APT:"
  read -p "1)Identifiant de l'utilisateur ? " login_apt
  read -p "2)Mot de passe de l'utilisateur ? " mdp_apt
  echo "Acquire::http::proxy \"http://$login_apt:$mdp_apt@$ip_proxy:$port_proxy/\";
Acquire::ftp::proxy \"ftp://$login_apt:$mdp_apt@$ip_proxy:$port_proxy/\";
Acquire::https::proxy \"https://$login_apt:$mdp_apt@$ip_proxy:$port_proxy/\";" > /etc/apt/apt.conf.d/10proxy

#Permettre d'utiliser la commande add-apt-repository derrière un Proxy
######################################################################
  echo "Defaults env_keep = https_proxy" >> /etc/sudoers

fi

# Modification pour ne pas avoir de problème lors du rafraichissement des dépots avec un proxy
# cette ligne peut être commentée/ignorée si vous n'utilisez pas de proxy ou avec la 14.04.
echo "Acquire::http::No-Cache true;" >> /etc/apt/apt.conf
echo "Acquire::http::Pipeline-Depth 0;" >> /etc/apt/apt.conf


# Vérification que le système est bien à jour
apt update ; 
#apt full-upgrade -y

# Ajout de Net-tools pour ifconfig en 18.04 et futures versions
apt install -y net-tools

#############################################
# Paquets nécessaires.
#############################################

apt install libpam-ldapd libpam-mount cifs-utils


#####################################################################
#Ajout d'une ligne dans /etc/pam.d/common-session 
#(activer le module PAM pam_mkhomedir.so)
######################################################################
l="session required        pam_mkhomedir.so skel=/etc/skel umask=0022"
if ! grep -q "pam_mkhomedir.so" /etc/pam.d/common-session; then
    sed -i "/pam_unix.so/a\\$l" /etc/pam.d/common-session
fi

######################################################################
#Ajout d'une ligne dans /etc/samba/smb.conf
######################################################################

if grep -q "^client min protocol" /etc/samba/smb.conf; then
    # il y a déjà un protocole ; on le met à jour
    sed -i 's/client min protocol.*/client min protocol = NT1/' /etc/samba/smb.conf
else
    # il n'y a pas encore de protocole ; on le crée en fin de fichier
    nb_ligne=$(grep -n "\[global\]" /etc/samba/smb.conf | cut -d ":" -f1)
    sed -i "$nb_ligne a\client min protocol=NT1" /etc/samba/smb.conf
fi

######################################################################
#Montage auto des partitions du serveur Kwartz
######################################################################
read -p "Quel est le nom du serveur Kwartz (souvent kwartz-server ou serveur) ?" nom_kwartz
perso="<volume user=\"*\" fstype=\"cifs\" server=\"$nom_kwartz\" path=\"%(USER)\" mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Espace_Perso\" options=\"vers=1.0\" />"
commun="<volume user=\"*\" fstype=\"cifs\" server=\"$nom_kwartz\" path=\"Commun\" mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Commun\" options=\"vers=1.0\" />"
public="<volume user=\"*\" fstype=\"cifs\" server=\"$nom_kwartz\" path=\"Public\" mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Public\" options=\"vers=1.0\" />"
grep "mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Espace_Perso\"" /etc/security/pam_mount.conf.xml  >/dev/null
if [ $? != 0 ]
then sed -i "/<\!-- Volume definitions -->/a\ $perso" /etc/security/pam_mount.conf.xml
else
  echo "Public déjà présent"
fi

grep "mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Commun\"" /etc/security/pam_mount.conf.xml  >/dev/null
if [ $? != 0 ]
then sed -i "/<\!-- Volume definitions -->/a\ $commun" /etc/security/pam_mount.conf.xml
else
  echo "Commun déjà présent"
fi

grep "mountpoint=\"/home/%(GROUP)/%(USER)/Bureau/Public\"" /etc/security/pam_mount.conf.xml  >/dev/null
if [ $? != 0 ]
then sed -i "/<\!-- Volume definitions -->/a\ $public" /etc/security/pam_mount.conf.xml
else
  echo "Espace_Perso déjà présent"
fi
#############################################
# Paquets pour le verrouillage clavier et mises à jour auto
#############################################
apt install -y numlockx unattended-upgrades oidentd
######################################################################
# activation auto des mises à jour de sécurité
######################################################################
echo "APT::Periodic::Update-Package-Lists \"1\";
APT::Periodic::Unattended-Upgrade \"1\";" > /etc/apt/apt.conf.d/20auto-upgrades

######################################################################
#/etc/profile
######################################################################
echo "
export LC_ALL=fr_FR.utf8
export LANG=fr_FR.utf8
export LANGUAGE=fr_FR.utf8
" >> /etc/profile

######################################################################
#protocole pour SMB
######################################################################
if grep -q "^client min protocol" /etc/samba/smb.conf; then
    # il y a déjà un protocole ; on le met à jour
    sed -i 's/client min protocol.*/client min protocol = NT1/' /etc/samba/smb.conf
else
    # il n'y a pas encore de protocole ; on le crée en fin de fichier
    echo "client min protocol = NT1" >> /etc/samba/smb.conf
fi


######################################################################
#ne pas créer les dossiers par défaut dans home
######################################################################
sed -i "s/enabled=True/enabled=False/g" /etc/xdg/user-dirs.conf

######################################################################
# les profs peuvent sudo
########################################################################
grep "%professeurs ALL=(ALL) ALL" /etc/sudoers > /dev/null
if [ $?!=0 ]
then
  sed -i "/%admin ALL=(ALL) ALL/a\%professeurs ALL=(ALL) ALL" /etc/sudoers
  sed -i "/%admin ALL=(ALL) ALL/a\%DomainAdmins ALL=(ALL) ALL" /etc/sudoers
else
  echo "prof déjà dans sudo"
fi

######################################################################
#Activation du shell 
######################################################################
ln -s /bin/bash /bin/kwartz-sh

# Suppression de paquet inutile sous Ubuntu/Unity
apt purge -y aisleriot gnome-mahjongg ;

# Pour être sûr que les paquets suivant (parfois présent) ne sont pas installés :
apt purge -y pidgin transmission-gtk gnome-mines gnome-sudoku abiword gnumeric thunderbird ;
apt purge -y mintwelcome ;

######################################################################
#nettoyage station avant clonage
######################################################################
apt-get -y autoremove --purge ; apt-get -y clean ; clear

######################################################################
#FIN
######################################################################
echo "C'est fini ! Un reboot est nécessaire..."
read -p "Voulez-vous redémarrer immédiatement ? [O/n] " rep_reboot
if [ "$rep_reboot" = "O" ] || [ "$rep_reboot" = "o" ] || [ "$rep_reboot" = "" ] ; then
  reboot
fi


