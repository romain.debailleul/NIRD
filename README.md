# Numérique Inclusif Responsable et Durable : NIRD


Le but de ce projet est d'acculturer les élèves et la communauté éducative aux logiciels libres. Nous proposons un retour d'expérience aux enseignants qui souhaitent s'engager dans la même démarche d'utilisation des logiciels libres dans leur établissement.

>À travers le projet NIRD les élèves acquièrent une culture numérique au sens large, qui associe les dimensions sociales et éthiques à la maîtrise d’habiletés techniques. 

 Il s'agit de faciliter le lancement de ce type de projet en indiquant la démarche que nous avons suivie et en offrant un retour d'expérience documenté attestant de la force et des avantages de l'utilisation des logiciels libres. En établissement scolaire et auprès des familles. 

Ressource technique, formation des enseignants, informations pratiques, cours pour les élèves. L'ensemble de la ressource se trouve sur [Maths-code.fr](https://maths-code.fr). En voici les composantes :

#### Intégrer un client GNU/Linux sur un réseau pédagogique Kwartz, utilisé dans les Hauts-de-France :
Le [script SHELL *kwartz-install*](Kwartz/kwartz-install.sh) permet d'inclure les machines GNU/Linux sur un réseau Kwartz :  réseau, annuaire LDAP, proxy. 

#### Documentation et retour d'expérience Logiciels libres au lycée

- [ ] [Retour d'expérience](https://maths-code.fr/cours/2022/06/28/logiciel-libre-au-lycee-retour-dexperience/) : écueils, informations pratiques, points clés de réussite.  
- [ ] [Description détaillée du projet](https://maths-code.fr/cours/numerique-responsable/) : les objectifs.


#### Reconditionnement de machines par les élèves pour les élèves en **100% logiciels libres**.
Le protocole ci-dessous est utilisé par les élèves du lycée Carnot de Bruay-Labuissière dans le cadre d’un reconditionnement de PCs rétrocédés par des entreprises, l’association de parents d’élèves ou des particuliers. 
> Ces machines reconditionnées sous GNU/Linux par les élèves sont à destination des élèves non équipés ou des écoles primaires.
    

En voici les grandes lignes :
- Nettoyage physique du PC : aspiration des poussières et soufflage pneumatique (bloquer les ventilateurs pour éviter les problèmes).
- Si nécessaire,installation d’un disque dur *SSD*, de barettes mémoires.
- Création de la clé USB « vive » à partir de l’image système téléchargée. Elle va permettre d’installer le système. Nous utilisons Ventoy pour créer une clé multiboot : plusieurs images de distribution peuvent y être stockées.
- En l’indiquant au BIOS/UEFI : démarrage sur la clé USB et installation du système à partir du système « live ». Il vous faut pour cela [appuyer sur la bonne touche au démarrage de la machine]().
- Éventuellement désactiver le *SecureBoot*.
- Paramétrage divers -pilotes de cartes graphiques, écran tactiles etc.- et installation des logiciels.

Voir le [Protocole complet de reconditionnement de machines sous **logiciels libres**](https://maths-code.fr/cours/recycler-un-pc-sous-gnu-linux/)

## Visuels
<img src="images/transparent_NIRD_bleu.png" width="150px">

## Installation
Pour utiliser le [script SHELL d'intégration à Kwartz](Kwartz/kwartz-install.sh) :
- Cliquer sur l'icône de téléchargement (à droite),
- ouvrir un terminal (ctrl+alt+T) dans le dossier de téléchargement,
- `chmod +x kwartz-install.sh` 
- ``/kwartz-install.sh``

Et appliquer la mise à jour des sources, puis celle des paquets :
- `sudo apt update`
- `sudo apt full-upgrade -y`

Cette dernière peut prendre un certain temps.

## Iso Linux Mint modifié collège/lycée
- [Lien de téléchargement](https://maths-code.fr/iso/NIRD_linuxmint-22-xfce-64bit-2024.12.07.iso) (Maths-code.fr)
- Somme sha256 : 8ef0c1b836c6ef03af2fc57d5c79285ab157223686563aa7ffd983115e0b5b85

## Documents : RSE, conventions.
- [Exemple d'audit du matériel sur tableur](https://maths-code.fr/NIRD/Documents/RSE_audit.ods) pour la Responsabilité sociale des entreprises. 
- [Convention école pour la donation](https://maths-code.fr/NIRD/Documents/Convention_ecole.odt)
- [Lettre-type demande de dons]

## Support
Adresse de contact : 
- romain.debailleul@ac-lille.fr
- pascal.beel@ac-lille.fr
- shirley.brochart@ac-lille.fr
## Contribution
Toute contribution est la bienvenue : documentation, script, remarques.

## Contributeurs
Pascal Beel, Romain Debailleul, Shirley Mikolajczak.
Merci à Emmanuel Ostenne, Rémi Debrock, Georges Khaznadar, Cédric Frayssinet pour leur aide.

## Licence
Le projet NIRD est placé sous licence creative commons CC-BY-SA pour les documents.
Le code est distribué avec la licence GPL 3.0.

## État du projet
### Lycée Carnot
La phase en établissement commencée en 2020 est terminée, ou plus précisément suit son cours, attestant par l'usage de l'efficacité des logiciels libres utilisés sous le système GNU/Linux en EPLE.
Toutefois, le changement de serveur Kwartz vers un serveur Windows pour les lycées des Hauts-de-France nous amène à considérer l'intégration des clients GNU/Linux sur ce type de serveur.

### Écoles primaires 
Le déploiement hors de l'établissement continue. Les écoles primaires de
- Pernes-en-Artois *contact : julien.brouet@ac-lille.fr* , 
- Marles-les-Mines école Camphin *contact : mathieu.thorel@ac-lille.fr*
Ajout de 10 PCs + 4 Pcs de l'école dual/boot.
- (Bruay-Labuissière école Jaurès *contact : sebastien.furmaniak@c-lille.fr*,
- Ecquedecques *contact : gwenaelle.fenet@ac-lille.fr*
- (Fouquières les Béthune)
- (Vendin-lès-Béthune)
- (Lespesses *contact : gwenaelle.fenet@ac-lille.fr*)
 

ont été équipées (ou sont en cours d'équipement). 
Les inspectrices et ERUN du bassin ont été reçues pour discuter du déploiement.

Trois collèges du bassin de Bruay-Labuissière ont rejoint le projet NIRD en ouvrant un club de reconditionnement sous logiciels libres :
- Rostand : *contact : michael.moreau@ac-lille.fr*
- Prévert : *contact : jean-francois.jedraszak@ac-lille.fr*
- Signoret : *contact : xavier.herbaut@ac-lille.Fr*
 
 Le lycée Dégrugillier à Auchel a effectué plusieurs déplacement pour que nos élèves forment les leurs au recondtionnement. Merci à Frédéric Lalloyer *contact : frederic.laloyer@ac-lille.fr* pour son investissement, incluant des éléments de mobilité pour ses élèves   
